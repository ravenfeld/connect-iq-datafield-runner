using Toybox.Application;

class DataFieldRunnerApp extends Application.AppBase {

	var view;
    function initialize() {
        AppBase.initialize();
        view = new DataFieldRunnerView();
    }

    // onStart() is called on application start up
    function onStart(state) {
    }

    // onStop() is called when your application is exiting
    function onStop(state) {
    	view.onStop(self, state);
    }

    // Return the initial view of your application here
    function getInitialView() {
        return [ view ];
    }

    function onSettingsChanged(){
        view.initialize();
    }
}