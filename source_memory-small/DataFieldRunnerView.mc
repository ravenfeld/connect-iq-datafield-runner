using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.Application as App;

class DataFieldRunnerView extends Ui.DataField {
 	hidden var unitKm;
 	hidden var avgPaceParameter;
 	hidden var errorFormat=false;
    hidden var heightFontMedium;
	hidden var heightFontTiny;
	hidden var text_width_max;
	hidden var distanceLapParameter=0;
	hidden var distanceLapFirstParameter=0;
	hidden var distanceGps;
	hidden var refueling;
	hidden var chrono=0;
	hidden var elapsedTime=0;
	hidden var timeLap=0;
	hidden var nbLap=0;
	hidden var timeLastLap=0;
	hidden var timerOnLap=0;
	hidden var distanceRunner=0;
	hidden var paceRunner=0;
	hidden var lapPace=0;
	hidden var lastLapPace=0;
	hidden var gapTime=0;
 	hidden var gapDistance=0;
	hidden var timerRunner=0;
	hidden var distanceGpsOnLap=0;
		
    function initialize() {
		unitKm = System.getDeviceSettings().distanceUnits==System.UNIT_METRIC;
		
		refueling = App.getApp().getProperty("refueling").toNumber();

		distanceLapParameter = App.getApp().getProperty("distance_lap").toFloat();
		if(!unitKm){
			distanceLapParameter=distanceLapParameter/3.281f;
		}
		distanceLapFirstParameter = App.getApp().getProperty("distance_lap_first").toFloat();
		if(!unitKm){
			distanceLapFirstParameter=distanceLapFirstParameter/3.281f;
		}
		if(distanceLapFirstParameter==0){
			distanceLapFirstParameter=distanceLapParameter;
		}
		
		var distanceParameter = App.getApp().getProperty("distance").toFloat()*1000;
        if(!unitKm){
			distanceParameter=distanceParameter*1.60934;
		}
		
		var timeExtract = extract(App.getApp().getProperty("time"));
		errorFormat=false;
		var allureExtract = extractPace(App.getApp().getProperty("pace"));
		if(allureExtract[0]==null || allureExtract[0]*60+allureExtract[1]==0){
			if(timeExtract[0]==null || timeExtract[1]==null || timeExtract[2]==null){
				errorFormat=true;
				avgPaceParameter = 0;
			}else{
				var time = 	(timeExtract[0]*60*60+timeExtract[1]*60+timeExtract[2])*1000;
				avgPaceParameter = time/distanceParameter;
			}
		}else{
			avgPaceParameter = allureExtract[0]*60+allureExtract[1];
			if(!unitKm){
				avgPaceParameter = 0.621371*avgPaceParameter;
			}
		}
		DataField.initialize();		
    }

	function extract(timeString){
		var timeExtract = new [3];
		var separatorIndex;
		for( var i=0;i<3;i++){
			separatorIndex = timeString.find(":");
			if(separatorIndex!=null){
				timeExtract[i] = timeString.substring(0, separatorIndex).toNumber();
				timeString = timeString.substring(separatorIndex+1,timeString.length());
			}else if(i==2 && timeExtract[0]!=null && timeExtract[1]!=null){
				timeExtract[i] = timeString.toNumber();
			}
		}
		return timeExtract;
	}
			
	function extractPace(timeString){
		var timeExtract = new [2];
		var separatorIndex;
		for( var i=0;i<2;i++){
			separatorIndex = timeString.find(":");
			if(separatorIndex!=null){
				timeExtract[i] = timeString.substring(0, separatorIndex).toNumber();
				timeString = timeString.substring(separatorIndex+1,timeString.length());
			}else if(i==1 && timeExtract[0]!=null){
				timeExtract[i] = timeString.toNumber();
			}
		}
		return timeExtract;
	}
	
	function onStop(app, state) {
	}
	
    function compute(info) {
    	chrono = info.timerTime;
    	elapsedTime = info.elapsedTime;
    	
    	if(App.getApp().getProperty("timer") == 0){
			timerRunner = elapsedTime;
		}else{
			timerRunner = info.timerTime;
		}
    	timeLap = timerRunner-timerOnLap;	
    	
    	distanceGps = info.elapsedDistance;
		var distanceLap = null;
		if(distanceGps != null){
			distanceLap = distanceGps-distanceGpsOnLap;
			if(nbLap==0){
				distanceRunner = distanceLap;
			}else{
				distanceRunner = distanceLapFirstParameter+(nbLap-1)*distanceLapParameter+distanceLap;
			}
		}

		if(timeLap!=null && distanceLap!=null && distanceLap!=0){
			lapPace = timeLap/distanceLap;		 
		}
		if(timerRunner!=null && distanceRunner!=0){
	 		paceRunner = timerRunner/distanceRunner;
	 	}else{
	 		distanceRunner=0;
			paceRunner=0;
			lapPace=0;
			lastLapPace=0;  
	 	}
	
		gapTime = (Math.round((paceRunner*distanceRunner-avgPaceParameter*distanceRunner)/1000)).toLong();
		if(paceRunner>0 && avgPaceParameter>0){
			gapDistance =  Math.round((timerRunner/paceRunner-timerRunner/avgPaceParameter));
		}    
    }
    
    function onLayout(dc) {
     	text_width_max = dc.getTextWidthInPixels("88.88km",Gfx.FONT_MEDIUM);
		heightFontMedium = dc.getFontHeight(Gfx.FONT_NUMBER_MEDIUM)*App.getApp().getProperty("sizeFont").toFloat();
		heightFontTiny = dc.getFontHeight(Gfx.FONT_SYSTEM_TINY)*App.getApp().getProperty("sizeFont").toFloat();
    }
    
    function onWorkoutStepComplete(){
    	onTimerLap();
    }
        
    function onTimerLap(){
		var isLap = null;
		if(nbLap==0){
			isLap= Math.round((distanceGps-distanceGpsOnLap)/distanceLapFirstParameter).toLong();  	
			if(isLap>1){
				var isLapFull= Math.round((distanceGps-distanceGpsOnLap-distanceLapFirstParameter)/distanceLapParameter).toLong();
				isLap=1+isLapFull;
				timeLastLap = (timerRunner-timerOnLap)/isLap;
				timerOnLap = timerRunner;
				distanceGpsOnLap=distanceGps;
				lastLapPace = timeLastLap/(distanceLapFirstParameter+distanceLapParameter*(isLapFull-1)).toFloat(); 
			}else{
				isLap=1;
				timeLastLap = (timerRunner-timerOnLap)/isLap;
				timerOnLap = timerRunner;
				distanceGpsOnLap=distanceGps;
				lastLapPace = timeLastLap/distanceLapFirstParameter.toFloat();
			}
		}else{
			isLap= Math.round((distanceGps-distanceGpsOnLap)/distanceLapParameter).toLong();
			if(isLap>0){
				timeLastLap = (timerRunner-timerOnLap)/isLap;
				timerOnLap = timerRunner;
				distanceGpsOnLap=distanceGps;
				lastLapPace = timeLastLap/(distanceLapParameter*isLap).toFloat();  
			}
		}
		nbLap=nbLap+isLap;
	} 
	
    function onUpdate(dc) {
    	dc.clear();
        var center_x = dc.getWidth()/2;
    	var center_y = dc.getHeight()/2;
    	var y = dc.getHeight()/3;
    	
    	dc.setColor(Gfx.COLOR_WHITE,Gfx.COLOR_WHITE);
		dc.fillRectangle(0,0,dc.getWidth(),dc.getHeight());
		dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_TRANSPARENT);
    	if(errorFormat){
    		dc.drawText(center_x, center_y, Gfx.FONT_SYSTEM_SMALL, "Error parsing format",  Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);				
    	}else{
    		var infoOnly= App.getApp().getProperty("info_only");
			if(infoOnly==0){
				displayInfo(dc,App.getApp().getProperty("info_top"),0, 0, dc.getWidth(), dc.getHeight()/3.0);
				displayInfo(dc,App.getApp().getProperty("info_left"),0, dc.getHeight()/3.0, dc.getWidth()/2.0, dc.getHeight()/3.0);
				displayInfo(dc,App.getApp().getProperty("info_right"),dc.getWidth()/2.0, dc.getHeight()/3.0, dc.getWidth()/2.0, dc.getHeight()/3.0);
				displayInfo(dc,App.getApp().getProperty("info_bottom"),0, dc.getHeight()/3.0*2.0, dc.getWidth(), dc.getHeight()/3.0+2);
				dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_WHITE);
				dc.drawLine(0, y, dc.getWidth(), y);
				dc.drawLine(center_x, y, center_x, y*2);
				dc.drawLine(0, y*2, dc.getWidth(), y*2);
			}else{
				displayInfo(dc,infoOnly-1,0, 0, dc.getWidth(), dc.getHeight());
			}
    	}
    }
					
	function displayInfo(dc,info,x, y, width, height){
		if(info==0){
			displayTextValue(dc,x, y, width, height,Ui.loadResource(Rez.Strings.Chrono),timeToString(chrono/1000));
		}else if(info==1){
			displayTextValue(dc,x, y, width, height,Ui.loadResource(Rez.Strings.ElaspsedTime),timeToString(elapsedTime/1000));
		}else if(info==2){
			displayTextValue(dc,x, y, width, height,Ui.loadResource(Rez.Strings.TimeLap),timeToString(timeLap/1000));
		}else if(info==3){
			displayNbLap(dc,x, y, width, height);			
		}else if(info==4){
			displayTextValue(dc,x, y, width, height,Ui.loadResource(Rez.Strings.LastLapTime),timeToString(timeLastLap/1000));
		}else if(info==5){
			displayTextValue(dc,x, y, width, height,Ui.loadResource(Rez.Strings.LastLapChrono),timeToString(timerOnLap/1000));
		}else if(info==6){
			displayDistance(dc,x, y, width, height,Ui.loadResource(Rez.Strings.Dist),distanceRunner);
		}else if(info==7){
			displayTextValue(dc,x, y, width, height,Ui.loadResource(Rez.Strings.AvgPace),timeToString(paceRunner.toLong()));
		}else if(info==8){
			displayTextValue(dc,x, y, width, height,Ui.loadResource(Rez.Strings.LastLapPace),timeToString(lapPace.toLong()));
		}else if(info==9){
			displayTextValue(dc,x, y, width, height,Ui.loadResource(Rez.Strings.LastLapPace),timeToString(lastLapPace.toLong()));
		}else if(info==10){
			displayGapTime(dc,x, y, width, height);
		}else if(info==11){
			displayGapDistance(dc,x, y, width, height);
		}
	}
	
	function displayTextValue(dc,x, y, width, height, text, value){
		dc.drawText(x+width/2, y+height/2-(heightFontMedium+heightFontTiny)/2+heightFontTiny/2, Gfx.FONT_SYSTEM_TINY, text,  Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);
		dc.drawText(x+width/2, y+height/2-(heightFontMedium+heightFontTiny)/2+heightFontTiny+heightFontMedium/2 ,Gfx.FONT_NUMBER_MEDIUM, value, Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);	
	}
	
	function displayDistance(dc,x, y, width, height, text, distance){
		dc.drawText(x+width/2, y+height/2-(heightFontMedium+heightFontTiny)/2+heightFontTiny/2, Gfx.FONT_SYSTEM_TINY, text,  Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);
		var distanceArray=distanceToString(Math.round(distance).toLong());
		dc.drawText(x+width/2, y+height/2-(heightFontMedium+heightFontTiny)/2+heightFontTiny+heightFontMedium/2 ,Gfx.FONT_NUMBER_MEDIUM, distanceArray[0], Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);
		dc.drawText(x+width/2+text_width_max/2, y+height/2-(heightFontMedium+heightFontTiny)/2+heightFontTiny+heightFontMedium/2 ,Gfx.FONT_SMALL, distanceArray[1],Graphics.TEXT_JUSTIFY_LEFT|Graphics.TEXT_JUSTIFY_VCENTER);	
	}
											
	function displayGapTime(dc,x, y, width, height){
		var gap="";
	 	if(gapTime>0){
	 		dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_WHITE);
			dc.fillRectangle(x, y, width, height);
			dc.setColor(Gfx.COLOR_WHITE,Gfx.COLOR_TRANSPARENT);
			gap=Ui.loadResource(Rez.Strings.RetardedTime);
		}else{
			gapTime=gapTime*-1;
			dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_TRANSPARENT);
			gap=Ui.loadResource(Rez.Strings.AdvancedTime);
		}
		displayTextValue(dc,x, y, width, height,gap,timeToString(gapTime));
	}
	
	function displayGapDistance(dc,x, y, width, height){
		var gap="";
	 	if(gapDistance>0){
			dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_TRANSPARENT);
			gap=Ui.loadResource(Rez.Strings.AdvancedDist);
		}else{
			gapDistance=gapDistance*-1;
			dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_WHITE);
			dc.fillRectangle(x, y, width, height);
			dc.setColor(Gfx.COLOR_WHITE,Gfx.COLOR_TRANSPARENT);
			gap=Ui.loadResource(Rez.Strings.RetardedDist);
		}
		displayDistance(dc,x, y, width, height,gap,gapDistance);
	}
	
	function displayNbLap(dc,x, y, width, height){
	 	if(nbLap>0 && refueling>0 && (nbLap+1)%refueling==0){
			dc.setColor(Gfx.COLOR_ORANGE,Gfx.COLOR_WHITE);
			dc.fillRectangle(x, y, width, height);
			dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_TRANSPARENT);
		}else if(nbLap>0 && refueling>0 && nbLap%refueling==0){
			dc.setColor(Gfx.COLOR_RED,Gfx.COLOR_WHITE);
			dc.fillRectangle(x, y, width, height);
			dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_TRANSPARENT);		
		}else{
			dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_TRANSPARENT);
		}
		displayTextValue(dc,x, y, width, height,Ui.loadResource(Rez.Strings.NbLap),nbLap);
	}
	
    function timeToString(long){
		var hour = long/60/60;
		if(hour>0){
			return hour+":"+((long / 60) % 60).format("%02d")+":"+(long % 60).format("%02d");
		}else{
			return ((long / 60) % 60)+":"+((long % 60)).format("%02d");
		}
	}
	
	function distanceToString(dist){
		var km = dist / 1000.0;
		if(km>=1){
			if (unitKm) {
				return [km.format("%0.2f"),"km" ];
			}else{	
				km=0.621371*km;
				return [km.format("%0.2f"),"mi" ];	
			}
		}else{
			if (unitKm) {
				return [dist.format("%d"),"m" ];	
			}else{	
				dist=3.28084*dist;
				return [dist.format("%d"),"ft" ];		
			}
		}
	}
}