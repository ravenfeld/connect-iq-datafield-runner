# Champs de donnée
    - Le chrono
    - Temps écoulé
    - Temps du lap
    - Le nombre de lap : le nombre de fois qu’on a appuyé sur lap
    - Le temps du lap précédent
    - Le chrono à la fin du lap précédent
    - Distance courante ajustée : la distance courante parcourue (celle rectifiée avec la règle de gestion des laps)
    - L’allure moyenne de la course (en utilisant la distance ajustée) 
    - L’allure moyenne du lap courant
    - L’allure du lap précédent (en utilisant la distance ajustée du lap précédent)
    - Avance/Retard en distance
    - Avance/Retard en temps


