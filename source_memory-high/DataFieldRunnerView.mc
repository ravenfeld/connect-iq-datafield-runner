using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.Application as App;

class DataFieldRunnerView extends Ui.DataField {

  	enum {
		PACE_ESTIMATE_DISTANCE_PROPERTY = 1
	}
	enum {
		TIMER_ON_LAP_PROPERTY = 2
	}
	enum {
		TIME_LAST_LAP_PROPERTY = 3
	}
	enum {
		DISTANCE_GPS_ON_LAP_PROPERTY = 4
	}	
	enum {
		LAST_LAP_PACE_PROPERTY = 5
	}
	enum {
		NB_LAB_PROPERTY = 6
	}
	enum {
		TIME_ESTIMATED_DISTANCE_PROPERTY = 7
	}
	
 	hidden var unitKm;
 	hidden var avgPaceParameter;
 	hidden var timeParameter;
 	hidden var errorFormat=false;
    hidden var heightFontMedium;
	hidden var heightFontTiny;
	hidden var text_width_max;
	hidden var distanceLapParameter=0;
	hidden var distanceLapFirstParameter=0;
	hidden var distanceLapParameterString=0;
	hidden var distanceLapFirstParameterString=0;
	hidden var timeParameterString;
	hidden var paceParameterString;
	hidden var isGoodPaceParameter;
	hidden var distanceParameter;
	hidden var distanceParameterString;
	hidden var distanceGps;
	hidden var infoTop;
	hidden var infoLeft;
	hidden var infoRight;
	hidden var infoBottom;
	hidden var refueling;
	hidden var isStart=false;

	hidden var chronoString;
	hidden var elaspsedTimeString;
	hidden var timeLapString;
	hidden var nbLapString;
	hidden var lastLapTimeString;
	hidden var lastLapChronoString;
	hidden var distanceString;
	hidden var avgPaceString;
	hidden var lapPaceString;
	hidden var lastLapPaceString;
	hidden var advancedTimeString;
	hidden var retardedTimeString;
	hidden var advancedDistString;
	hidden var retardedDistString;
	hidden var estimatedDistString;
	hidden var estimatedChronoLapString;
	
	hidden var startPaceString;
	hidden var startDistString;	
	hidden var startTpsString;	
	hidden var startDistLap1String;	
	hidden var startDistLapString;	
	hidden var startRefuelingString;	
	hidden var startReadyString;
	hidden var startPressStartString;		
	
	hidden var chrono=0;
	hidden var elapsedTime=0;
	hidden var timeLap=0;
	hidden var nbLap=0;
	hidden var timeLastLap=0;
	hidden var timerOnLap=0;
	hidden var distanceRunner=0;
	hidden var paceRunner=0;
	hidden var paceEstimatedDistance=0;
	hidden var lapPace=0;
	hidden var lastLapPace=0;
	hidden var gapTime=0;
 	hidden var gapDistance=0;
	hidden var timerRunner=0;
	hidden var distanceGpsOnLap=0;
	hidden var estimatedDistance=0;
	hidden var estimatedChronoLap=0;
	hidden var timesEstimatedDistance;
	hidden var start=true;
		
    function initialize() {
        var metric = System.getDeviceSettings().distanceUnits;
		unitKm = metric==System.UNIT_METRIC;
		
		chronoString= Ui.loadResource(Rez.Strings.Chrono);
	 	elaspsedTimeString= Ui.loadResource(Rez.Strings.ElaspsedTime);
		timeLapString= Ui.loadResource(Rez.Strings.TimeLap);
		nbLapString= Ui.loadResource(Rez.Strings.NbLap);
		lastLapTimeString= Ui.loadResource(Rez.Strings.LastLapTime);
		lastLapChronoString= Ui.loadResource(Rez.Strings.LastLapChrono);
		distanceString= Ui.loadResource(Rez.Strings.Dist);
		avgPaceString= Ui.loadResource(Rez.Strings.AvgPace);
		lapPaceString= Ui.loadResource(Rez.Strings.LapPace);
		lastLapPaceString= Ui.loadResource(Rez.Strings.LastLapPace);
		advancedTimeString= Ui.loadResource(Rez.Strings.AdvancedTime);
		retardedTimeString= Ui.loadResource(Rez.Strings.RetardedTime);
		advancedDistString= Ui.loadResource(Rez.Strings.AdvancedDist);
		retardedDistString= Ui.loadResource(Rez.Strings.RetardedDist);
		estimatedDistString= Ui.loadResource(Rez.Strings.EstimatedDist);
		estimatedChronoLapString= Ui.loadResource(Rez.Strings.EstimatedChronoLap);		
		startPaceString= Ui.loadResource(Rez.Strings.startPace);
		startDistString= Ui.loadResource(Rez.Strings.startDist);	
		startTpsString= Ui.loadResource(Rez.Strings.startTps);
		startDistLap1String= Ui.loadResource(Rez.Strings.startDistLap1);	
		startDistLapString= Ui.loadResource(Rez.Strings.startDistLap);
		startRefuelingString= Ui.loadResource(Rez.Strings.startRefueling);
		startReadyString= Ui.loadResource(Rez.Strings.startReady);
		startPressStartString= Ui.loadResource(Rez.Strings.startPressStart);	
		
		infoTop = App.getApp().getProperty("info_top");
		infoLeft = App.getApp().getProperty("info_left");
		infoRight = App.getApp().getProperty("info_right");
		infoBottom = App.getApp().getProperty("info_bottom");

		refueling = App.getApp().getProperty("refueling").toNumber();
		timesEstimatedDistance =  new [refueling];
		
		distanceLapParameterString = App.getApp().getProperty("distance_lap");
		distanceLapParameter = distanceLapParameterString.toFloat();
		if(!unitKm){
			distanceLapParameter=distanceLapParameter/3.281f;
		}
		distanceLapFirstParameterString = App.getApp().getProperty("distance_lap_first");
		distanceLapFirstParameter = distanceLapFirstParameterString.toFloat();
		if(!unitKm){
			distanceLapFirstParameter=distanceLapFirstParameter/3.281f;
		}
		if(distanceLapFirstParameter==0){
			distanceLapFirstParameter=distanceLapParameter;
		}
		
		distanceParameterString = App.getApp().getProperty("distance");
		distanceParameter=distanceParameterString.toFloat()*1000;
        if(!unitKm){
			distanceParameter=distanceParameter*1.60934;
		}
		
		timeParameterString = App.getApp().getProperty("time");
		var timeExtract = extract(timeParameterString);
		errorFormat=false;
		paceParameterString = App.getApp().getProperty("pace");				
		var allureExtract = extractPace(paceParameterString);
		if(allureExtract[0]==null || allureExtract[0]*60+allureExtract[1]==0){
			isGoodPaceParameter=false;
			if(timeExtract[0]==null || timeExtract[1]==null || timeExtract[2]==null){
				errorFormat=true;
				avgPaceParameter = 0;
			}else{
				timeParameter = (timeExtract[0]*60*60+timeExtract[1]*60+timeExtract[2])*1000;
				avgPaceParameter = timeParameter/distanceParameter;
			}
		}else{
			isGoodPaceParameter=true;
			avgPaceParameter = allureExtract[0]*60+allureExtract[1];
			if(!unitKm){
				avgPaceParameter = 0.621371*avgPaceParameter;
			}
		}
		DataField.initialize();		
    }

	function extract(timeString){
		var timeExtract = new [3];
		var separatorIndex;
		for( var i=0;i<3;i++){
			separatorIndex = timeString.find(":");
			if(separatorIndex!=null){
				timeExtract[i] = timeString.substring(0, separatorIndex).toNumber();
				timeString = timeString.substring(separatorIndex+1,timeString.length());
			}else if(i==2 && timeExtract[0]!=null && timeExtract[1]!=null){
				timeExtract[i] = timeString.toNumber();
			}
		}
		return timeExtract;
	}
			
	function extractPace(timeString){
		var timeExtract = new [2];
		var separatorIndex;
		for( var i=0;i<2;i++){
			separatorIndex = timeString.find(":");
			if(separatorIndex!=null){
				timeExtract[i] = timeString.substring(0, separatorIndex).toNumber();
				timeString = timeString.substring(separatorIndex+1,timeString.length());
			}else if(i==1 && timeExtract[0]!=null){
				timeExtract[i] = timeString.toNumber();
			}
		}
		return timeExtract;
	}
	
	function onStop(app, state) {
		app.setProperty(PACE_ESTIMATE_DISTANCE_PROPERTY, paceEstimatedDistance);
		app.setProperty(TIMER_ON_LAP_PROPERTY, timerOnLap);
		app.setProperty(TIME_LAST_LAP_PROPERTY, timeLastLap);
		app.setProperty(DISTANCE_GPS_ON_LAP_PROPERTY, distanceGpsOnLap);
		app.setProperty(LAST_LAP_PACE_PROPERTY, lastLapPace);
		app.setProperty(NB_LAB_PROPERTY, nbLap);
		var value="";
		for( var i = 0; i < timesEstimatedDistance.size(); i++ ) {
			if(timesEstimatedDistance[i]!=null){
				if(i==0){
					value= timesEstimatedDistance[i].toString();
				}else{
					value= value +":"+ timesEstimatedDistance[i];
				}
			}
		}
		app.setProperty(TIME_ESTIMATED_DISTANCE_PROPERTY, value);
	}
	
    function compute(info) {
    	chrono = info.timerTime;
    	elapsedTime = info.elapsedTime;
    	if(start && elapsedTime>0){
    		paceEstimatedDistance = App.getApp().getProperty(PACE_ESTIMATE_DISTANCE_PROPERTY);
    		if(paceEstimatedDistance==null){
    			paceEstimatedDistance=0;
    		}
    		timerOnLap = App.getApp().getProperty(TIMER_ON_LAP_PROPERTY);
    		if(timerOnLap==null){
    			timerOnLap=0;
    		}
    		timeLastLap = App.getApp().getProperty(TIME_LAST_LAP_PROPERTY);
    		if(timeLastLap==null){
    			timeLastLap=0;
    		}
    		distanceGpsOnLap = App.getApp().getProperty(DISTANCE_GPS_ON_LAP_PROPERTY);
    		if(distanceGpsOnLap==null){
    			distanceGpsOnLap=0;
    		}
    		lastLapPace = App.getApp().getProperty(LAST_LAP_PACE_PROPERTY);
    		if(lastLapPace==null){
    			lastLapPace=0;
    		}
    		nbLap = App.getApp().getProperty(NB_LAB_PROPERTY);
    		if(nbLap==null){
    			nbLap=0;
    		}
    		var value= App.getApp().getProperty(TIME_ESTIMATED_DISTANCE_PROPERTY);
    		var separatorIndex;
    		if(value!=null){
    			var end=false;
    			 var i=0;
    			while(!end && i<timesEstimatedDistance.size()){
					separatorIndex = value.find(":");
					if(separatorIndex!=null){
						timesEstimatedDistance[i] = value.substring(0, separatorIndex).toNumber();
						value = value.substring(separatorIndex+1,value.length());
					}else{
						timesEstimatedDistance[i] = value.toNumber();
						end=true;
					}
					i++;
				}
			}
		}
    	start=false;
    	
    	if(App.getApp().getProperty("timer") == 0){
			timerRunner = elapsedTime;
		}else{
			timerRunner = info.timerTime;
		}
    	timeLap = timerRunner-timerOnLap;	
    	
    	var distanceLap = null;
   		distanceGps = info.elapsedDistance;
		if(distanceGps != null){
			distanceLap = distanceGps-distanceGpsOnLap;
			if(App.getApp().getProperty("using_lap")==0){
				distanceRunner=distanceGps;
			}else{			
				if(nbLap==0){
					distanceRunner = distanceLap;
				}else{
					distanceRunner = distanceLapFirstParameter+(nbLap-1)*distanceLapParameter+distanceLap;
				}
			}
		}

		if(timeLap!=null && distanceLap!=null && distanceLap!=0){
			lapPace = timeLap/distanceLap;		 
		}
		if(timerRunner!=null && distanceRunner!=0){
	 		paceRunner = timerRunner/distanceRunner;
	 	}else{
	 		distanceRunner=0;
			paceRunner=0;
			lapPace=0;
			lastLapPace=0;  
	 	}

		if(timeParameter != null){
			if(paceEstimatedDistance>0 && App.getApp().getProperty("using_lap")!=0){
				estimatedDistance = distanceRunner + (timeParameter-timerRunner)/paceEstimatedDistance;
			}else if(paceRunner>0){
				estimatedDistance = distanceRunner + (timeParameter-timerRunner)/paceRunner;
			}
		}
		if(timeParameter != null){
			estimatedChronoLap=Math.round((timeParameter-timerRunner)*distanceLapParameter/(distanceParameter-distanceRunner));
		}	
		gapTime = (Math.round((paceRunner*distanceRunner-avgPaceParameter*distanceRunner)/1000)).toLong();
		if(paceRunner>0 && avgPaceParameter>0){
			gapDistance =  Math.round((timerRunner/paceRunner-timerRunner/avgPaceParameter));
		}    
	}
    
    function onLayout(dc) {
     	text_width_max = dc.getTextWidthInPixels("88.88km",Gfx.FONT_MEDIUM);
		heightFontMedium = dc.getFontHeight(Gfx.FONT_NUMBER_MEDIUM)*App.getApp().getProperty("sizeFont").toFloat();
		heightFontTiny = dc.getFontHeight(Gfx.FONT_SYSTEM_TINY)*App.getApp().getProperty("sizeFont").toFloat();
    }
    
    function onWorkoutStepComplete(){
    	onTimerLap();
    }
    
    function onTimerStart(){
    	isStart=true;
    }
    
    function onTimerLap(){
		var isLap = null;
		if(nbLap==0){
			if(App.getApp().getProperty("using_lap")==1){
				isLap= Math.round((distanceGps-distanceGpsOnLap)/distanceLapFirstParameter).toLong();			
			}else{
				isLap=1;
			}  	
			if(isLap>1){
				var isLapFull= Math.round((distanceGps-distanceGpsOnLap-distanceLapFirstParameter)/distanceLapParameter).toLong();
				isLap=1+isLapFull;
				timeLastLap = (timerRunner-timerOnLap)/isLap;
				timerOnLap = timerRunner;
				distanceGpsOnLap=distanceGps;
				lastLapPace = timeLastLap/(distanceLapFirstParameter+distanceLapParameter*(isLapFull-1)).toFloat(); 
			}else{
				isLap=1;
				timeLastLap = (timerRunner-timerOnLap)/isLap;
				timerOnLap = timerRunner;
				distanceGpsOnLap=distanceGps;
				lastLapPace = timeLastLap/distanceLapFirstParameter.toFloat();
			}
		}else{
			if(App.getApp().getProperty("using_lap")==1){
				isLap= Math.round((distanceGps-distanceGpsOnLap)/distanceLapParameter).toLong();
			}else{
				isLap=1;
			}  
			if(isLap>0){
				timeLastLap = (timerRunner-timerOnLap)/isLap;
				timerOnLap = timerRunner;
				distanceGpsOnLap=distanceGps;
				lastLapPace = timeLastLap/(distanceLapParameter*isLap).toFloat();  
			}
		}
		
		nbLap=nbLap+isLap;


		if(isLap>0){
			var tempTimers= new [refueling];
			for( var i = 0; i < refueling; i += 1 ) {
				if(i<isLap){
   					tempTimers[i] = timeLastLap;
   				}else{
   					tempTimers[i] = timesEstimatedDistance[(i-isLap).toNumber()];
   				}
			}
			for( var i = 0; i < refueling; i += 1 ) {
				timesEstimatedDistance[i]=tempTimers[i];
			}		
				
			var distance;
			var nbLapEstimatedDistance;
			if(nbLap>refueling){
				nbLapEstimatedDistance = refueling;
				distance = refueling*distanceLapParameter;
			}else{
				nbLapEstimatedDistance = nbLap;
				distance = distanceLapFirstParameter+(nbLap-1)*distanceLapParameter;
			}
			var totalTimes=0;
		
			for( var i = 0; i < nbLapEstimatedDistance; i += 1 ) {
				if(timesEstimatedDistance[i]!=null){
   				 	totalTimes= totalTimes + timesEstimatedDistance[i];
   				 }
			}
			var timerEstimatedDistance = totalTimes/nbLapEstimatedDistance;
			paceEstimatedDistance = totalTimes/distance;
		}
	} 	
	
    function onUpdate(dc) {
    	dc.clear();
        var center_x = dc.getWidth()/2;
    	var center_y = dc.getHeight()/2;
    	var y = dc.getHeight()/3;
    	
    	dc.setColor(Gfx.COLOR_WHITE,Gfx.COLOR_WHITE);
		dc.fillRectangle(0,0,dc.getWidth(),dc.getHeight());
		dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_TRANSPARENT);
    	if(errorFormat){
    		dc.drawText(center_x, center_y, Gfx.FONT_SYSTEM_SMALL, "Error parsing format",  Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);				
    	}else if(isStart){
    		var infoOnly= App.getApp().getProperty("info_only");
			if(infoOnly==0){
				displayInfo(dc,infoTop,0, 0, 0+5, dc.getWidth(), dc.getHeight()/3.0);
				displayInfo(dc,infoLeft,0, dc.getHeight()/3.0,dc.getHeight()/3.0-3, dc.getWidth()/2.0, dc.getHeight()/3.0);
				displayInfo(dc,infoRight,dc.getWidth()/2.0, dc.getHeight()/3.0,dc.getHeight()/3.0-3, dc.getWidth()/2.0, dc.getHeight()/3.0);
				displayInfo(dc,infoBottom,0, dc.getHeight()/3.0*2.0, dc.getHeight()/3.0*2.0-5, dc.getWidth(), dc.getHeight()/3.0+2);
				dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_WHITE);
				dc.drawLine(0, y, dc.getWidth(), y);
				dc.drawLine(center_x, y, center_x, y*2);
				dc.drawLine(0, y*2, dc.getWidth(), y*2);
			}else{
				displayInfo(dc,infoOnly-1,0, 0, dc.getWidth(), dc.getHeight());
			}
    	}else{
 			var x = dc.getWidth()/5;
 			var y = dc.getHeight()/7;
 			var unit;
 			if(isGoodPaceParameter){
  				dc.drawText(x, y ,Gfx.FONT_SYSTEM_TINY, startPaceString+paceParameterString, Graphics.TEXT_JUSTIFY_LEFT|Graphics.TEXT_JUSTIFY_VCENTER);		
 			}else{
 				
 				if (unitKm) {
 					unit="km";
 				}else{
 					unit="mi";
 				}
 				dc.drawText(x, y,Gfx.FONT_SYSTEM_TINY, startDistString+distanceParameterString+ " "+unit, Graphics.TEXT_JUSTIFY_LEFT|Graphics.TEXT_JUSTIFY_VCENTER);
 				y =y+heightFontTiny;
 				dc.drawText(x, y,Gfx.FONT_SYSTEM_TINY, startTpsString+timeParameterString, Graphics.TEXT_JUSTIFY_LEFT|Graphics.TEXT_JUSTIFY_VCENTER);	
 			}
 			if (unitKm) {
 				unit="m";
 			}else{
 				unit="ft";
 			}
 			y =y+heightFontTiny;
 			dc.drawText(x, y,Gfx.FONT_SYSTEM_TINY, startDistLap1String+distanceLapFirstParameterString+ " "+unit, Graphics.TEXT_JUSTIFY_LEFT|Graphics.TEXT_JUSTIFY_VCENTER);
 			y =y+heightFontTiny;
 			dc.drawText(x, y,Gfx.FONT_SYSTEM_TINY, startDistLapString+distanceLapParameterString+ " "+unit, Graphics.TEXT_JUSTIFY_LEFT|Graphics.TEXT_JUSTIFY_VCENTER);
			y =y+heightFontTiny;
			dc.drawText(x, y,Gfx.FONT_SYSTEM_TINY, startRefuelingString+refueling, Graphics.TEXT_JUSTIFY_LEFT|Graphics.TEXT_JUSTIFY_VCENTER);
			y =y+heightFontTiny/2.5;
			var lapCorrector="";
			if(App.getApp().getProperty("using_lap")==0){
				lapCorrector=Ui.loadResource(Rez.Strings.No);
			}else if(App.getApp().getProperty("using_lap")==1){
				lapCorrector=Ui.loadResource(Rez.Strings.LapCorrector);
			}else{
				lapCorrector=Ui.loadResource(Rez.Strings.Lap);
			}
			dc.drawText(x, y,Gfx.FONT_SYSTEM_TINY, lapCorrector, Graphics.TEXT_JUSTIFY_LEFT);
			y =dc.getHeight() - dc.getHeight()/6;
			dc.drawLine(0, y, dc.getWidth(), y);
			y =y+heightFontTiny/2;
			dc.drawText(center_x, y,Gfx.FONT_SYSTEM_TINY, startPressStartString, Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);
		
    	}
    }
					
	function displayInfo(dc,info,x, y, y_text, width, height){
		if(info==0){
			displayTextValue(dc,x, y_text, width, height,chronoString,timeToString(chrono/1000));
		}else if(info==1){
			displayTextValue(dc,x, y_text, width, height,elaspsedTimeString,timeToString(elapsedTime/1000));
		}else if(info==2){
			displayTextValue(dc,x, y_text, width, height,timeLapString,timeToString(timeLap/1000));
		}else if(info==3){
			displayNbLap(dc,x, y, y_text, width, height);			
		}else if(info==4){
			displayTextValue(dc,x, y_text, width, height,lastLapTimeString,timeToString(timeLastLap/1000));
		}else if(info==5){
			displayTextValue(dc,x, y_text, width, height,lastLapChronoString,timeToString(timerOnLap/1000));
		}else if(info==6){
			displayDistance(dc,x, y_text, width, height,distanceString,distanceRunner);
		}else if(info==7){
			displayTextValue(dc,x, y_text, width, height,avgPaceString,timeToString(paceRunner.toLong()));
		}else if(info==8){
			displayTextValue(dc,x, y_text, width, height,lapPaceString,timeToString(lapPace.toLong()));
		}else if(info==9){
			displayTextValue(dc,x, y_text, width, height,lastLapPaceString,timeToString(lastLapPace.toLong()));
		}else if(info==10){
			displayGapTime(dc,x, y, y_text, width, height);
		}else if(info==11){
			displayGapDistance(dc,x, y, y_text, width, height);
		}else if(info==12){
			displayDistance(dc,x, y, width, height,estimatedDistString,estimatedDistance);
		}else if(info==13){
			displayTextValue(dc,x, y_text, width, height,estimatedChronoLapString,timeMsToString(estimatedChronoLap.toLong()/100));
		}
	}
	
	function displayTextValue(dc,x, y, width, height, text, value){
		dc.drawText(x+width/2, y+height/2-(heightFontMedium+heightFontTiny)/2+heightFontTiny/2, Gfx.FONT_SYSTEM_TINY, text,  Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);
		dc.drawText(x+width/2, y+height/2-(heightFontMedium+heightFontTiny)/2+heightFontTiny+heightFontMedium/2 ,Gfx.FONT_NUMBER_MEDIUM, value, Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);	
	}
	
	function displayDistance(dc,x, y, width, height, text, distance){
		dc.drawText(x+width/2, y+height/2-(heightFontMedium+heightFontTiny)/2+heightFontTiny/2, Gfx.FONT_SYSTEM_TINY, text,  Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);
		var distanceArray=distanceToString(Math.round(distance).toLong());
		dc.drawText(x+width/2, y+height/2-(heightFontMedium+heightFontTiny)/2+heightFontTiny+heightFontMedium/2 ,Gfx.FONT_NUMBER_MEDIUM, distanceArray[0], Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);
		dc.drawText(x+width/2+text_width_max/2, y+height/2-(heightFontMedium+heightFontTiny)/2+heightFontTiny+heightFontMedium/2 ,Gfx.FONT_SMALL, distanceArray[1],Graphics.TEXT_JUSTIFY_LEFT|Graphics.TEXT_JUSTIFY_VCENTER);	
	}
											
	function displayGapTime(dc,x, y, y_text, width, height){
		var gap="";
	 	if(gapTime>0){
	 		dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_WHITE);
			dc.fillRectangle(x, y, width, height);
			dc.setColor(Gfx.COLOR_WHITE,Gfx.COLOR_TRANSPARENT);
			gap=retardedTimeString;
		}else{
			gapTime=gapTime*-1;
			dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_TRANSPARENT);
			gap=advancedTimeString;
		}
		displayTextValue(dc,x, y_text, width, height,gap,timeToString(gapTime));
		dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_TRANSPARENT);
	}
	
	function displayGapDistance(dc,x, y, y_text, width, height){
		var gap="";
	 	if(gapDistance>0){
			dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_TRANSPARENT);
			gap=advancedDistString;
		}else{
			gapDistance=gapDistance*-1;
			dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_WHITE);
			dc.fillRectangle(x, y, width, height);
			dc.setColor(Gfx.COLOR_WHITE,Gfx.COLOR_TRANSPARENT);
			gap=retardedDistString;
		}
		displayDistance(dc,x, y_text, width, height,gap,gapDistance);
		dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_TRANSPARENT);
	}
	
	function displayNbLap(dc,x, y, y_text, width, height){
	 	if(nbLap>0 && refueling>0 && (nbLap+1)%refueling==0){
			dc.setColor(Gfx.COLOR_ORANGE,Gfx.COLOR_WHITE);
			dc.fillRectangle(x, y, width, height);
			dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_TRANSPARENT);
		}else if(nbLap>0 && refueling>0 && nbLap%refueling==0){
			dc.setColor(Gfx.COLOR_DK_BLUE,Gfx.COLOR_WHITE);
			dc.fillRectangle(x, y, width, height);
			dc.setColor(Gfx.COLOR_WHITE,Gfx.COLOR_TRANSPARENT);		
		}else{
			dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_TRANSPARENT);
		}
		displayTextValue(dc,x, y_text, width, height,nbLapString,nbLap);
		dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_TRANSPARENT);
	}
	
    function timeToString(long){
		var seconds = long % 60;
		var minutes = (long / 60) % 60;
		var hour = long/60/60;
		if(hour>0){
			return hour+":"+minutes.format("%02d")+":"+seconds.format("%02d");
		}else{
			return minutes+":"+seconds.format("%02d");
		}
	}
	
	function timeMsToString(long){
		var milliseconds = long%10;
		var seconds = (long/10) % 60;
		var minutes = (long / 10 / 60) % 60;
		var hour = long/10/60/60;
		if(hour>0){
			return hour+":"+minutes.format("%02d")+":"+seconds.format("%02d")+"."+milliseconds;
		}else{
			return minutes+":"+seconds.format("%02d")+"."+milliseconds;
		}
	}
	
	function distanceToString(dist){
		var km = dist / 1000.0f;
		if(km>=1){
			if (unitKm) {
				return [km.format("%0.2f"),"km" ];
			}else{	
				km=0.621371*km;
				return [km.format("%0.2f"),"mi" ];	
			}
		}else{
			if (unitKm) {
				return [dist.format("%d"),"m" ];	
			}else{	
				dist=3.281*dist;
				return [dist.format("%d"),"ft" ];		
			}
		}
	}
}